function showTime (){
    let date= new Date();

    let hours = date.getHours();
    let min = date.getMinutes();
    let seconds = date.getSeconds();
    let session = "AM";

    if (hours === 0) {
        hours = 12;
    };

    if (hours > 12) {
        hours= hours-12;
        session = "PM";
    }

    hours = (hours < 10)? `0${hours}` : hours;
    min = (min < 10) ? `0${min}` : min;
    seconds = (seconds <10) ? `0${seconds}` :seconds;

    let time= `${hours}:${min}:${seconds} ${session}`;

    document.querySelector(".time").innerText=time;
}

setInterval (showTime , 1000);